import json
import os
from pathlib import Path

import pandas as pd

from region_rubricator_module.exceptions import *
from region_rubricator_module.schemas import (AnalysisResultDto, AnalysisResultState, PredictDto)
from region_rubricator_module.utils import clean_and_lemmatize, raise_flag_for_exception_words

class RegionRubricatorModule:
    name = "RegionRubricatorModule"
    version = "v2.0.0"

    def __init__(self, update_json=False):
        try:
            file_dir = Path(__file__).resolve().parents[0]
            self.modelpath = os.path.join(file_dir, "models")
        except Exception:
            raise NoModelPathError()

        # update_json=True is used when the excel file with keywords is updated
        self.update_json = update_json

        if self.update_json:
            try:
                df = pd.read_excel(os.path.join(self.modelpath, "cities.xlsx"))
                self.result_dict = {}

                for index, row in df.iterrows():
                    keys = [row['Город'], row['Мужчина'], row['Женщина'], row['Граждане'], row["Прилагательное"]]
                    keys_lem = ["None" if pd.isna(val) else clean_and_lemmatize(val) for val in keys]
                    keys = ["None" if pd.isna(val) else val.capitalize() for val in keys]
                    for key, key_lem in zip(keys, keys_lem):
                        value = row['Класс']
                        self.result_dict[key_lem] = {"key":key, "class":value}
                del self.result_dict["None"]
                with open(os.path.join(self.modelpath, "cities.json"), "w") as f:
                    json.dump(self.result_dict, f, ensure_ascii=False)

            except Exception:
                raise ModelReadingError()

        with open(os.path.join(self.modelpath, "cities.json")) as f:
            self.result_dict = json.load(f)


    def find_keywords(self, processed_text: str) -> [list, list]:
        """
        Searches for matches in the input text and the dictionary of keywords
        :param processed_text: lemmatized and cleaned input text
        :return: lists with found keywords and corresponding regions
        """
        tokenized_text = processed_text.split()
        found_keywords, predicted_regions = [], []
        for indx,token in enumerate(tokenized_text):
            if token in self.result_dict.keys():
                if not raise_flag_for_exception_words(tokenized_text, token):
                    if (token == 'новгород') & (indx > 0) & (tokenized_text[indx-1] == 'нижний'):
                        token = 'нижний новгород'
                        self.result_dict['нижний новгород'] = {"key":'Нижний Новгород', "class":'Нижний Новгород'}
                    if self.result_dict[token]["key"] not in found_keywords:
                        found_keywords.append(self.result_dict[token]["key"])
                    if self.result_dict[token]["class"] not in predicted_regions:
                        predicted_regions.append(self.result_dict[token]["class"])
        return found_keywords, predicted_regions

    def predict(self, input_text: str) -> AnalysisResultDto:
        try:
            result = self.__call__(input_text)
            result = PredictDto(is_region=result["is_region"], keywords=result["keywords"], region=result["region"])
            return AnalysisResultDto(state=AnalysisResultState.success.value, result=result)
        except Exception as e:
            return AnalysisResultDto(state=AnalysisResultState.error.value, description=f"{type(e)}: {e}")

    def __call__(self, text: str) -> dict:
        """
        Main method to determine region of the input text.

        Arguments:
            text (str): input text
        Returns:
            result (dict):
                - is_region (bool): region flag
                - keywords (list): found keywords
                - region (list): predicted region
        """
        if not isinstance(text, str):
            raise InputTextFormatError
        if len(text) == 0:
            raise NoneTextError
        if len(text.split()) > 10000:
            raise MaxTextLengthError

        result = {"is_region": False, "keywords": [], "region": []}

        try:
            processed_text = clean_and_lemmatize(text)
        except Exception:
            raise TextProcessingError

        found_keywords, predicted_regions = self.find_keywords(processed_text)

        if len(found_keywords) > 0:
            result["is_region"] = True
            result["keywords"] = found_keywords
            result["region"] = predicted_regions

        return result


if __name__ == "__main__":
    obj = RegionRubricatorModule(update_json=False)
    text = "Kyokushin Karate Satoryschool Личный канал: https:\/\/t.me\/+Fs8CUPPD7dVlODBi Сатори Школа Чемпионов Каратэ Киокушин Поддержать канал ₽: Сбер 40817810938123165789 Москва: ~ ул. 2я Вольская 11, к. 4 Пн, Ср, Пт 16:00-22:00"
    print(obj.predict(text))
