class BaseRubricatorException(ValueError):
    def __str__(self):
        return f"{self.txt}"


class NoModelPathError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Нет директории с файлами модели"


class ModelReadingError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Ошибка чтения словаря"


class MaxTextLengthError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Длина входного текста больше 10000 символов"


class NoneTextError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"На входе пустая строка"


class InputTextFormatError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Неправильный формат входного текста!"


class TextProcessingError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Ошибка при обработке входного текста!"


class ResultOutputError(BaseRubricatorException):
    def __init__(self):
        self.txt = f"Ошибка вывода результатов поиска в консоль!"
