from enum import Enum
from typing import Literal, Optional
from pydantic import BaseModel

# class InputDto(BaseModel):
#     text: str


class AnalysisResultState(Enum):
    success = "success"
    error = "error"


class PredictDto(BaseModel):
    is_region: bool
    keywords: list
    region: list


class AnalysisResultDto(BaseModel):
    state: str = Literal[
        AnalysisResultState.success.value,
        AnalysisResultState.error.value,
    ]
    result: Optional[PredictDto]
