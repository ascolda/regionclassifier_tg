import re
import pymorphy2
from string import punctuation

BLACKLIST = set(punctuation)
BLACKLIST.add("«")
BLACKLIST.add("»")
morph = pymorphy2.MorphAnalyzer()

#сокращения, которые важны для исключения нерелеванта
WHITELIST = ["ул", "пл"]


def clean_and_lemmatize(text: str) -> str:
    """Removes punctuation, digits and stopwords. Lemmatizes the input string
    """
    non_russian_pattern = re.compile(r"[^а-яА-ЯёЁ\s]")

    text = str(text)
    text = text.lower()
    text = text.replace("\xa0", " ")
    text = text.replace("/", " ")
    text = text.replace("\\", " ")
    text = text.replace("-", " ")
    text = text.replace(".", " ")
    text = non_russian_pattern.sub("", text)
    words = text.split()
    res = []
    for word in words:
        word = "".join(c for c in word if (c not in BLACKLIST and c.isdigit() is False))
        if (len(word) >= 3) or (word in WHITELIST):
            word = morph.parse(word)[0].normal_form
            res.append(word)

    return (" ").join(res)

def raise_flag_for_exception_words(tokenized_text: str, token:str) -> bool:
 	""" Check whether the found token if related to an address entity and should not be taken into account
 	"""
    exception_words_before_keyword = ["пер", "ул", "пл", "улица"]
    exception_words_after_keyword = ["пер", "переулок", "проезд", "проспект", "район", "округ"]

    indx = tokenized_text.index(token)

    if (indx > 0) and (tokenized_text[indx - 1] in exception_words_before_keyword):
        return True
    if (indx != len(tokenized_text) - 1) and (tokenized_text[indx + 1] in exception_words_after_keyword):
        return True

    return False



if __name__ == "__main__":
    text = "Kyokushin Karate Satoryschool Личный канал: https:\/\/t.me\/+Fs8CUPPD7dVlODBi Сатори Школа Чемпионов Каратэ Киокушин Поддержать канал ₽: Сбер 40817810938123165789 Москва: ~ ул. 2я Вольская 11, к. 4 Пн, Ср, Пт 16:00-22:00"
    tokenized_text = clean_and_lemmatize(text).split()
    print(tokenized_text)
    print(raise_flag_for_exception_words(tokenized_text, "вольский"))
