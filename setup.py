from setuptools import (
    setup,
    find_packages
)

__version__: str = "2.0.0"

with open("requirements/main.txt") as f:
    install_requires = f.read().strip().split("\n")

setup(
    name="region_rubricator",
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires
)
