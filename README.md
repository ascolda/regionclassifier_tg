# region-rubricator-module

Модуль предназначен для анализа описаний телеграм-каналов. 
Если описание содержит в себе признак территориальной принадлежности, то выполняется классификация канала с определением региона Российской Федерации.
Для модуля был составлен словарь городов России и этнохоронимов (названия жителей городов и регионов. Например, тюменец, крымчане)

## Разработка

- необходимо установить виртуальное окружение в папку venv
- устанавливаем все зависимости из requirements
```bash
  pip install -r requirements/dev.txt
```
## Тесты
* Для запуска тестов наберите команду
```bash
  python -m unittest tests.test_module
```

## Пример использования
```python
from region_rubricator_module.module import RegionRubricatorModule

obj = RegionRubricatorModule(update_json=False)
text = "Призрак Цыбульского Канал о работе врио губернатора Архангельской области		Для связи @newsarh29"
print(obj.predict(text))

>>> state='success' result=PredictDto(is_region=True, keywords=['архангельский'], region=['Архангельск'])
```
