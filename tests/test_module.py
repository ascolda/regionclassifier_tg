import unittest
from region_rubricator_module.module import RegionRubricatorModule


class RegionRubricatorModuleTestCase(unittest.TestCase):
    def test_init_model(self):
        analyzer = RegionRubricatorModule()
        assert (analyzer.result_dict['петрозаводчанин'] == {'key': 'Петрозаводчане', 'class': 'Петрозаводск'})

    def test_predict_region(self):
        text = """Стерлитамак | Люди | Разное Новости Вашего города!\t
                Всё самое интересное, новое, важное!\t\tПо рекламе пишите @dinara_chis"""
        analyzer = RegionRubricatorModule()
        result_dto = analyzer.predict(text)
        self.assertEqual(result_dto.state, "success")
        self.assertEqual(result_dto.result.is_region, True)
        self.assertEqual(result_dto.result.keywords, ['Стерлитамак'])
        self.assertEqual(result_dto.result.region, ['Уфа'])

    def test_exception_for_streets(self):
        text = """KСПК27 🩸 Краевая станция переливания крови ❣️г.Хабаровск, ул. Волочаевская,46 (тел.36-60-39) 
                    г.Комсомольск-на-Амуре, ул. Севастопольская,53 🏥 Прием доноров"""
        analyzer = RegionRubricatorModule()
        result_dto = analyzer.predict(text)
        self.assertEqual(result_dto.state, "success")
        self.assertEqual(result_dto.result.is_region, True)
        self.assertEqual(result_dto.result.keywords, ['Хабаровск', 'Комсомольск'])
        self.assertEqual(result_dto.result.region, ['Хабаровск'])

    def test_not_a_string_at_input(self):
        text = 1325
        analyzer = RegionRubricatorModule()
        result_dto = analyzer.predict(text)
        self.assertEqual(result_dto.state, "error")
        self.assertEqual(result_dto.result, None)

    def test_empty_string_at_input(self):
        text = ""
        analyzer = RegionRubricatorModule()
        result_dto = analyzer.predict(text)
        self.assertEqual(result_dto.state, "error")
        self.assertEqual(result_dto.result, None)
